//alert ("Hello");

//Assignment Operators

//Basic assignment operator (=)
	//It allows us to assign a value to a variable.

	let variable = "initial value";

//Mathematical Operators (addition (+), subtraction (-), multiplication (*), division (/), modulo (%))

let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

	//addition Assignment Operator (+=)
	//left operand is the variable or the value of the left side of the operator
	//right operand is the variable or the value of the right side of the operator
	//num1 = num1 + num4 (re-assigned the value of num1 with the result of num1 + num4)

	num1 += num4;
	console.log(num1);
	//result: 45

	num1 += 55;
	console.log(num1);
	//result:100
	console.log(num4);
	//result: 40;

	let string1 = "Boston ";
	let string2 = "Celtics ";
	string1 += string2;
	console.log(string1);
	//Result: Boston Celtics
	console.log(string2);
	//result: Celtics

	/*15 += num1;
	console.log(num1);*/
	//result:error
	//note: do not use assignment operator when the left operand is just data

	//Subtraction Assignment Operator

	num1 -= num2;
	console.log(num1);
	//result: 90
	num1 -= num4;
	console.log(num1);
	//result: 50
	num1 -= 10;
	console.log(num1);
	//result: 40
	num1 -= string1;
	console.log(num1);
	//result: NaN (Not a Number) because string1 is an alphanumeric string
	string1 -= string2;
	console.log(string1);
	//result:NaN

	// Multiplication Assignment Operator

	num2 *= num3;
	console.log(num2);
	//result: 40
	num2 *= 5;
	console.log(num2);
	//result: 200

	//Division Assignment Operator

	num4 /= num3;
	console.log(num4);
	//result: 10
	num4 /= 2;
	console.log(num4);
	//result: 5

	//Modulo OPerator (%)
	/*console.log(num2);
		//result:5
	console.log(num4);
		//result:200
	console.log(num4 % num2);
		//result: 0*/

	/*let y = 10;
	let x = 2
	console.log(y % x);
	//result: 0*/

	let y = 15;
	let x = 2
	console.log(y % x);
	//result: 1


//Mathematical Operations - we follow MDAS (multiplication, division, addition, subtraction)

let mdasResult = 1 + 2 - 3 * 4 / 5
console.log(mdasResult);
//result: 0.6

//PEMDAS - Parenthesis, exponents, multiplication, division, addition, subtraction

let pemdasResult = 1 + (2-3) * (4/5);
console.log(pemdasResult);
 //result: 0.2

 //Increment and Decrement

 	//2 kinds of incrementation: pre-fix and post-fix

 let z = 1;

 //pre-fix incrementation

 ++z;
 console.log(z);
 	//result: 2
 	//the value of z was added with 1 and is immediately returned

 //post-fix incrementation
 z++;
 console.log(z);
 	//result: 3 - the value of Z is added with 1
 console.log(z++);
 	//result: 3 - post incrementation the previous value of the variable is returned first before incrementing
 console.log(z);
 	//result: 4 - the new value is referenced

//pre-fix and post-fix decrementation
	console.log(--z);
	//result: 3 - the result subtracted by 1 and is returned imediately
	console.log(z--);
	//result:3 - returned the previous value
	console.log(z);
	//result:2 - returned the new value

//Comparison Operators
	//this is used to compare the value of the left and right operand
	//note that comparison operators return a boolean

	//Equality or loose equality operators (==)

	console.log(1 == 1);
		//result: true

	//we can also save the results of a comparison in a variable

	let isSame = 55 == 55;
	console.log(isSame);
	//result: true

	console.log(1 == "1");
	//result: true
	//note: loose equality prioritize the sameness of the value, forced coercion is done before comparison. forced conversion - JS forcibly changes the data type of the operands.

	console.log(0 == false);
	//result: true 
	//note: false is converted into a number and the equivalent of false is 0

	console.log(1 == true);
	//result: true
	//note: true is equal to 1

	console.log('1' == 1);
	//result = true

	console.log(true == "true");
	//result = false
	//note: true is equal to 1 while "true" is a string that does not convert

	/*
		With loose comparison operators (==), values are compared and types, if operands do not have the same type, it will be forced coerced before comparison of values

		If either operand is a number or boolean, the operands are converted into number

	*/

	//Strict Equality Operator (===)
	console.log(true === "1");
	//result: false
	//note: checks both the value and the data type

	console.log("Johnny" === "Johnny");
	//result: true
	//note: same data type and same value

	console.log("lisa" === "Lisa");
	//result: false
	//note: differences with letter casing can affect the value

//inequality Operators
	//Loose inequality Operators (!=)
		//checks whether the operands are NOT equal and/or have different values.
		//will do type coercion if the operands have different data types

	console.log("1" != 1);
	//result: false
	//"1" is converted to number 1
	//1 is converted to 1
	//1 equals to 1
	//not true

	console.log("Rose" != "Jennie");
		//result: true

	console.log(false != 0);
		//result: false
		//note: false will be converted to 0

	console.log(true != "true");
		//result: true

	//Strict Inequality Operators (!==)
		//It will check whether the two operands have different values and will check if they have different date types

	console.log("5" !== 5);
		//result: true
		//note: they have different data types

	console.log(5 !== 5);
		//result: false
		//note: they have the same value and same data type

	console.log("true" !== true);
		//result: true
		//note: different date types


let name1 = "Juan";
let name2 = "Shane";
let name3 = "Peter";
let name4 = "Jack";

let number = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";


console.log(numString1 == number);
// true
console.log(numString1 === number);
// false
console.log(numString1 != number);
// false
console.log(name4 !== name3);
// true
console.log(name1 == "juan");
// false
console.log(name1 === "Juan");
// true

//Relational Comparison Operator
	//A comparison operator which will check the relationship between the operands

	let q = 500;
	let r = 700;
	let w = 8000;
	let numString3 = "5500";

	//Greater Than (>)

	console.log(q > r);
		//result: false
	console.log(w > r);
	 	//result: true

	//Less Than (<)

	console.log(w < q);
		//result: false
	console.log(q < 1000);
		//result: true
	console.log(numString3 < 6000);
		//result: true
	console.log(numString3 < "Jose");
		//result: true - this is erractic

	//Greater Than or equal to (>=)

	console.log(w >= 8000);
		//result: true
	console.log(r >= q);
		//result: true


	//Less Than or Equal To (<=)

	console.log(q <= r);
		//result: true
	console.log(w <= q);
		//result: false


	// Logical Operators
		// And Operator (&&)
			//Both operands on the left and right or all operands added must be true or will result to true.

		let isAdmin = false;
		let isRegistered = true;
		let isLegalAge = true;

		let authorization1 = isAdmin && isRegistered;
		console.log(authorization1);
			//result: False

		let authorization2 = isLegalAge && isRegistered;
		console.log(authorization2);
			//result: true

		let requiredLevel = 95;
		let requiredAge = 18;
		let authorization3 = isRegistered && requiredLevel === 25;
		console.log(authorization3);
			//result: false
			//note: true && 95 === 25 which will result to false
			//true && false will be false

		let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
		console.log(authorization4);
			//result: true

		let userName = "gamer2001";
		let userName2 = "shadow1991";
		let userAge = 15;
		let userAge2 = 30;

		let registration1 = userName.length > 8 && userAge >= requiredAge;
		console.log(registration1)
			//result: False

		let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
		console.log(registration2);
			//result: true

		//Or Operator(|| - double pipe)
			//Or operator returns true if at least one of the operands are true

		let userLevel1 = 100;
		let userLevel2 = 65;

		let guildRequirement1 = isRegistered && userLevel1 >= requiredLevel && userAge >= requiredAge;

		console.log(guildRequirement1);
		//result: false

		let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
		console.log(guildRequirement2);
		//result: true

		let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
		console.log(guildAdmin);
		//result: false

	//Not Operator (!)
		//turns a boolean value into the opposite value

		console.log(!isRegistered);
		//result: false
		console.log(!guildAdmin);
		//result: True

//If-else Statements
	
	//If statement will run a block of code if the condition specified is true or results to true

	/*if(true){
		alert('We run an if Condition!')
	}*/

	let userName3 = "crusader_1993";
	let userLevel3 = 25;
	let userAge3 = 20;

	if(userName3.length > 10){
		console.log("Welcome to Game Online!")
	};
	if(userLevel3 >= requiredLevel){
		console.log("You are qualified to join the Guild!")
	};

	if(userName3.length >= 10 && isRegistered && isAdmin){
		console.log("Thank you for joining the admin!")
	};


	//Else Statement
		//will run if the condition given is flase or results to false

	if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the Noob Guild!");
} else {
	console.log("You are too strong to be a Noob :(")
};

// else-if
	//executes a statement if the previous or the original condition is false or resulted to false but another specified condition resulted to true.

	if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
		console.log("Thank you for joining the Noob Guild!");
	} else if (userLevel3 > 25){
		console.log("You are too strong to be a Noob");
	} else if (userAge3 < requiredAge){
		console.log("You are too young to join the Guild");
	} else if(userName3.length < 10){
		console.log("Username is too short");
	} else {
		console.log("Unable to Join")
	};

	//if-else inside a function

	function addNum(num1, num2){
		if(typeof num1 === "number" && typeof num2 === "number"){

			console.log("Run only if both arguments passed are number types");
			console.log(num1 + num2);
		} else {
			console.log("One or both of the arguments are not numbers");
		};
	};

	addNum(5, 2);


/*function login(username, password){
		if(typeof username === "string" && typeof password === "string"){

			console.log("Both Arguments are string");

				if(username.length >= 8 && password.length >= 8){
					alert("Thank you for logging in")
				} else if(username.length < 8){
					alert("Username is too short")
				} else if(password.length < 8){
					alert("Password is too short")
				}

		} else {
			console.log("One of the arguments is not a string type.");
		}
	};

	login("theTinker", "tinkerbell");*/

	//Switch Statements
		// is an alternative to an if, else-if, else tree, where the data being evaluated or checked is of an expected input.

		//Syntax:
		/*
			switch (expression/condition){
				case value:
					statement;
					break;
				default:
					statement;
					break;

			}
		*/

	let hero = "Jose Rizal";

	switch(hero){

		case "Jose Rizal":
		console.log("National Hero of the Philippines");
		break;

		case "George Washington":
		console.log("Hero of the American Revolution");
		break;

		case "Hercules":
		console.log("Lengendary hero of the Greeks");
		break;
	};

function roleChecker(role){

	switch(role){
		case "Admin":
			console.log("Welcome Admin");
			break;

		case "User":
			console.log("Welcome User");
			break;

		case "Guest":
			console.log("Welcome Guest!");

		default:
			console.log("Invalid role");
	}
};
roleChecker("Guest");

function gradeEvaluator(grade){

	if(grade >= 90){
		return "A"
	} else if (grade >= 80){
		return "B"
	} else if (grade >= 70){
		return "C"
	} else if (grade <= 70){
		return "F"
	} else {
		return "Invalid Grade"
	}
};
	let letterDistinction = gradeEvaluator(85);
	console.log(letterDistinction);
	//result:


//Ternary Operator
	// A shorthand way of writing if-else statements
	/*
	Syntax:
	condition ? if-statement : else-statement
	*/

	let price = 5000;
	price > 1000 ? console.log("Price is over 1000") : 
		console.log("Price is less than 1000");

	let villain = "Harvey Dent";

	//villain === "Harvey Dent" ? console.log("You were supposed to be the chosen one");
		//note: else statement in ternary operations is required

	villain === "Two Face"
	? console.log("You lived long enough to be the villain")
	: console.log("Not quite villainous yet")

	//note: Ternary Operators are not meant for complex if-else trees. However, the main advantage of ternary operator is not because it's short, rather ternary operation implicitly returns or it can return without the return keyword

	let robin1 = "Dick Grayson";
	let currentRobin = "Tim Drake";

	let isFirstRobin = currentRobin === robin1 ? true : false;
	console.log(isFirstRobin);
		//result: False

	//Else-If with ternary Operator

	let a = 7
	a === 5
	? console.log("A")
	: console.log(a === 10 ? console.log("A is 10") :
		console.log("A is not 5 or 10"));
